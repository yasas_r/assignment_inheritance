package com.nCinga;

public class Packing implements Department {

    private   int countOfProduct;

    public Packing(){
        countOfProduct = 0;
    }

    public Packing(int countOfProduct ){
        setCountOfProduct(countOfProduct);
    }

    @Override
    public void increaseProduct() {
        setCountOfProduct(this.countOfProduct+5);

    }

    @Override
    public void decreaseProduct() {

        setCountOfProduct(this.countOfProduct-5);

    }

    private void setCountOfProduct(int countOfProduct){

        if(countOfProduct>=0)
            this.countOfProduct = countOfProduct;
        else
            this.countOfProduct = 0;
    }

    public int getPackingCountOfProduct(){
        return this.countOfProduct;
    }

    public void printPackingProductCount(){
        System.out.println("product count of Swing: "+ getPackingCountOfProduct());
    }
}
