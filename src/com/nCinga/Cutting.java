package com.nCinga;

public class Cutting implements Department{

    private int countOfProduct;

    public Cutting(){
        countOfProduct = 0;
    }

    public Cutting(int countOfProduct ){
        setCountOfProduct(countOfProduct);
    }

    @Override
    public void increaseProduct() {
        setCountOfProduct(this.countOfProduct+1);
    }

    @Override
    public void decreaseProduct(){
        setCountOfProduct(this.countOfProduct-1);
    }

    private void setCountOfProduct(int countOfProduct){

        if(countOfProduct>=0)
            this.countOfProduct = countOfProduct;
        else
            this.countOfProduct = 0;
    }

    public int getCuttingCountOfProduct(){
        return this.countOfProduct;
    }

    public void printCuttingProductCount(){
        System.out.println("product count of Swing: "+ getCuttingCountOfProduct());
    }
}
