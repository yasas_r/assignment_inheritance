package com.nCinga;

public class Sewing implements Department{

    private int countOfProduct;

    public Sewing(){
        countOfProduct = 0;
    }

    public Sewing(int countOfProduct ){
        setCountOfProduct(countOfProduct);
    }

    @Override
    public void increaseProduct() {

        setCountOfProduct(this.countOfProduct+1);

    }

    @Override
    public void decreaseProduct() {

        setCountOfProduct(this.countOfProduct-1);

    }

    public void printSewingProductCount(){
        System.out.println("product count of Swing: "+ getSewingCountOfProduct());
    }

    private void setCountOfProduct(int countOfProduct){

        if(countOfProduct>=0)
            this.countOfProduct = countOfProduct;
        else
            this.countOfProduct = 0;
    }

    public int getSewingCountOfProduct(){
        return this.countOfProduct;
    }
}
