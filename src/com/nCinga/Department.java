package com.nCinga;

public interface Department {

    void increaseProduct();
    void decreaseProduct();
}
