package com.nCinga;

public class Main {

    public static void main(String[] args) {

        Cutting cutting = new Cutting();

        cutting.increaseProduct();
        cutting.increaseProduct();
        cutting.increaseProduct();
        cutting.printCuttingProductCount();

        Sewing sewing = new Sewing();
        sewing.increaseProduct();
        sewing.printSewingProductCount();

        Packing packing = new Packing();
        packing.increaseProduct();
        packing.increaseProduct();
        packing.decreaseProduct();
        packing.printPackingProductCount();


        //increase();

    }

        //increase the product of all 3 departments
    public static void increase(){
        Cutting cutting = new Cutting();
        cutting.increaseProduct();
        cutting.printCuttingProductCount();

        Sewing sewing = new Sewing();
        sewing.increaseProduct();
        sewing.printSewingProductCount();

        Packing packing = new Packing();
        packing.increaseProduct();
        packing.printPackingProductCount();
    }

        //decrease the product of all 3 departments
    public static void decrease(){
        Cutting cutting = new Cutting();
        cutting.decreaseProduct();
        cutting.printCuttingProductCount();

        Sewing sewing = new Sewing();
        sewing.decreaseProduct();
        sewing.printSewingProductCount();

        Packing packing = new Packing();
        packing.decreaseProduct();
        packing.printPackingProductCount();
    }
}
